import React, {Component} from 'react';
import axios from 'axios/index';
import {baseUrl, token} from '../api/Api';


class LoginForm extends Component {
    state = {
        username: '',
        password: '',
        authenticated: false
    };

    getApi = (name, pass) => {
        axios.post(baseUrl + token, {username: name, password: pass})
            .then(res => {
                if (res.status === 200) {
                    localStorage.setItem('token', JSON.stringify(res.data.token));
                    this.props.onSuccessfullLogin();
                }
            })
            .catch(error => {
                console.log(error);
                const alert = document.querySelector('.alert');
                alert.classList.remove('alert-hidden');
            })
    };

    handleChangeName = event => {
        this.setState({username: event.target.value});
    };

    handleChangePass = event => {
        this.setState({password: event.target.value});
    };

    handleSubmit = event => {
        event.preventDefault();
        this.getApi(this.state.username, this.state.password);
    };


    render() {
        return (
            <div className='form-wrap'>
                <form onSubmit={this.handleSubmit}>
                    <div className='form-group'>
                        <label>Login</label>
                        <input type='text' className='form-control' placeholder='enter login'
                               onChange={this.handleChangeName}/>
                    </div>
                    <div className='form-group'>
                        <label>Password</label>
                        <input type='password' className='form-control' placeholder='enter password'
                               onChange={this.handleChangePass}/>
                    </div>
                    <button className='btn btn-primary' type='submit'>Войти</button>
                </form>
                <div className='alert alert-hidden alert-danger' role='alert'>
                    Неправильно введеные данные, повторите попытку
                </div>
            </div>
        )
    }
}

export default LoginForm;