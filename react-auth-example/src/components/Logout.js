import React, {Component} from 'react';
import {Redirect} from 'react-router-dom';


class Logout extends Component {

    handleLogout = () => {
        localStorage.removeItem('token');
        this.setState({authorized: false});
    };

    isAuthenticated = () => {
        const getToken = localStorage.getItem('token');
        if (getToken && getToken.length > 10) {
            return true;
        }
    };

    render() {
        const isAllReadyAuth = this.isAuthenticated();

        return (
            <div>
                {!isAllReadyAuth ? <Redirect to={{pathname: '/'}}/> : (
                    <div className="logout-row">
                        <button className='btn-logout btn btn-primary' type='submit' onClick={this.handleLogout}>Выйти</button>
                    </div>
                )}
            </div>
        )
    }
}


export default Logout;