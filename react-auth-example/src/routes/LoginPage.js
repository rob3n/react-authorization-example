import React, {Component} from 'react';
import {Redirect} from 'react-router-dom';
import LoginForm from '../components/LoginForm';


class LoginPage extends Component {
    state = {

    };

    handleSuccessLogin = () => {
        this.setState({authenticated: true})
    };

    isAuthenticated = () => {
        const getToken = localStorage.getItem('token');
        if (getToken && getToken.length > 10) {
            return true;
        }
    };

    render() {
        const isAllReadyAuth = this.isAuthenticated();

        return (
            <div>
                {isAllReadyAuth ? <Redirect to={{pathname: '/main'}}/> : (
                    <div>
                        <LoginForm onSuccessfullLogin={this.handleSuccessLogin}/>
                    </div>
                )}
            </div>
        )
    }
}


export default LoginPage;