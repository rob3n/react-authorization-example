import React, { Component } from 'react';
import {Redirect} from 'react-router-dom';
import {token} from '../api/Api';
import Logout from '../components/Logout'

class HomePage extends Component {

    isAuthenticated = () => {
        const getToken = localStorage.getItem('token');
        if(getToken && token.length > 10) {
            return true;
        }
    };

    render() {
        const isAllReadyAuth = this.isAuthenticated();
        return (
            <div>
                {!isAllReadyAuth ? <Redirect to='/error' /> :(
                    <div className='homepage-component'>
                        <h1 className='main-page-title'>Вы авторизовались!</h1>
                        <Logout />
                    </div>
                )}
            </div>
        )
    }
}

export default HomePage;