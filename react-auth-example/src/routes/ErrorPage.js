/* eslint-disable linebreak-style */
import React, {Component} from 'react'

class ErrorPage extends Component {
    render() {
        return (
            <div className='error-page-component'>
                <h1>Вы не являетесь пользователем.</h1>
            </div>
        )
    }
}

export default ErrorPage;