/* eslint-disable linebreak-style */
import React, {Component} from 'react';
import {BrowserRouter as Router, Route, Link} from 'react-router-dom';
import MainPage from './routes/MainPage';
import LoginPage from './routes/LoginPage';
import ErrorPage from './routes/ErrorPage';


class App extends Component {

    state = {

    };

    render() {
        return (
            <Router>
                <div className='main-wrap'>
                    <section className='test-navigation'>
                        <div className='container'>
                            <div className='row'>
                                <div className='col-10 offset-1 col-md-6 offset-md-3'>
                                    <ul className='nav justify-content-center'>
                                        <li className='nav-item nav-link'>
                                            <Link to='/main'>MainPage</Link>
                                        </li>
                                        <li className='nav-item nav-link'>
                                            <Link to='/'>Login</Link>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </section>
                    <section className='content'>
                        <div className='container'>
                            <div className='row'>
                                <div className='col-10 offset-1 col-md-6 offset-md-3'>
                                    <Route exact path='/' component={LoginPage} />
                                    <Route exact path='/main' component={MainPage}/>
                                    <Route exact path='/error' component={ErrorPage}/>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>
            </Router>
        )
    }
}

export default App;
